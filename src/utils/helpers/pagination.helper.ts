export type paginationData = {
  page: number,
  per_page: number,
  skip: number
};

export function fixPaginationData(page: number, per_page: number): paginationData {
  if (!page)
    page = 1;

  if (!per_page)
    per_page = 10;

  let skip = (page - 1) * per_page;

  return {
    skip,
    page,
    per_page
  };
}