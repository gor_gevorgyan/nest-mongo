import { Types } from 'mongoose';
import { BadRequestException } from '@nestjs/common';

export function checkIdAndThrow(id: string) {
  let id_is_correct = Types.ObjectId.isValid(id);

  if (!id_is_correct)
    throw new BadRequestException('invalid id');
}