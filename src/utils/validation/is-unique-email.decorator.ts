import {
  registerDecorator,
  ValidationOptions,
  isEmail
} from 'class-validator';
import { connect } from 'mongoose';

export function IsUniqueEmail(validationOptions?: ValidationOptions) {
  return function(object: Object, propertyName: string) {
    registerDecorator({
      name: 'IsOnlyDate',
      target: object.constructor,
      propertyName: propertyName,
      constraints: [],
      options: {
        message: 'email exists',
        ...validationOptions,
      },
      validator: {
        async validate(value: any) {
          if(!isEmail(value))
            return true;

          let mongo_connect = await connect(process.env.DB_HOST, {
            useNewUrlParser: true
          });
          let connection = mongo_connect.connection;

          let user = await connection
            .collection('users')
            .findOne({
              email: value.toLowerCase()
            });

          await connection.close();

          return !user;
        },
      },
    });
  };
}
