import { Module } from '@nestjs/common';
import { UserModule } from './resources/user/user.module';
import { CodeModule } from './resources/code/code.module';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule } from '@nestjs/config';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true
    }),
    MongooseModule.forRoot(process.env.DB_HOST),
    UserModule,
    CodeModule,
    AuthModule
  ],
})
export class AppModule {}
