import { BadRequestException, Injectable } from '@nestjs/common';
import { CreateCodeDto } from './dto/create-code.dto';
import { UpdateCodeDto } from './dto/update-code.dto';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { checkIdAndThrow } from '../../utils/helpers/check.helper';
import { paginationData } from '../../utils/helpers/pagination.helper';
import { BarCode, BarCodeDocument } from '../../schemas/bar-code.schema';
import { UserDocument } from '../../schemas/user.schema';

@Injectable()
export class CodeService {
  constructor(@InjectModel(BarCode.name) private barCodeModel: Model<BarCodeDocument>) {}

  async create(user: UserDocument, createCodeDto: CreateCodeDto) {
    let code = await this.barCodeModel.create({
      value: createCodeDto.value,
      type: createCodeDto.type,
      description: createCodeDto.description,
      userId: user._id
    });

    return {
      success: true,
      message: 'created successfully',
      data: {
        _id: code._id,
        type: code.type,
        value: code.value,
        description: code.description
      }
    };
  }

  async findAll(user: UserDocument, pagination: paginationData) {
    let codes = await this.barCodeModel
      .find({
        userId: user
      })
      .select({
        _id: 1,
        type: 1,
        value: 1,
        description: 1
      })
      .skip(pagination.skip)
      .limit(pagination.per_page);

    return codes;
  }

  async findOne(user: UserDocument, id: string) {
    checkIdAndThrow(id);

    let code = await this.barCodeModel
      .findOne({
        _id: id,
        userId: user._id
      })
      .select({
        _id: 1,
        type: 1,
        value: 1,
        description: 1
      });

    if (!code)
      throw new BadRequestException('invalid id or not exists');

    return code;
  }

  async update(user: UserDocument, id: string, updateCodeDto: UpdateCodeDto) {
    checkIdAndThrow(id);

    if (!Object.keys(updateCodeDto).length)
      throw new BadRequestException('no data for update');

    let update_data: UpdateCodeDto = {};

    if (updateCodeDto.description)
      update_data.description = updateCodeDto.description;

    if (updateCodeDto.type)
      update_data.type = updateCodeDto.type;

    if (updateCodeDto.value)
      update_data.value = updateCodeDto.value;

    let update = await this.barCodeModel.updateOne({
      _id: id
    }, update_data, {
      runValidators: true
    });

    if (!update.n)
      throw new BadRequestException('invalid id or not exists')

    return {
      success: true,
      message: 'updated successfully'
    };
  }

  async remove(user: UserDocument, id: string) {
    checkIdAndThrow(id);

    let delete_code = await this.barCodeModel.deleteOne({
      _id: id
    });

    if (!delete_code.deletedCount)
      throw new BadRequestException('invalid item id');

    return {
      success: true,
      message: 'deleted successfully',
      data: delete_code
    };
  }
}
