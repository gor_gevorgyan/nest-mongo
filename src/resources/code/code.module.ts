import { Module } from '@nestjs/common';
import { CodeService } from './code.service';
import { CodeController } from './code.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { BarCode, BarCodeSchema } from '../../schemas/bar-code.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: BarCode.name,
        schema: BarCodeSchema
      }
    ])
  ],
  controllers: [CodeController],
  providers: [CodeService]
})
export class CodeModule {}
