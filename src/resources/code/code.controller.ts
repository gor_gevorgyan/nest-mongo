import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  UseGuards,
  Query,
  Request
} from '@nestjs/common';
import { CodeService } from './code.service';
import { CreateCodeDto } from './dto/create-code.dto';
import { UpdateCodeDto } from './dto/update-code.dto';
import { JwtAuthGuard } from '../../auth/jwt-auth.guard';
import { fixPaginationData } from '../../utils/helpers/pagination.helper';

@Controller('code')
@UseGuards(JwtAuthGuard)
export class CodeController {
  constructor(private readonly codeService: CodeService) {}

  @Get()
  findAll(
    @Request() req,
    @Query('page') page: number = 1,
    @Query('per_page') per_page: number = 10
  ) {
    return this.codeService.findAll(req.user, fixPaginationData(+page, +per_page));
  }

  @Post()
  create(
    @Request() req,
    @Body() createCodeDto: CreateCodeDto
  ) {
    return this.codeService.create(req.user, createCodeDto);
  }

  @Get(':id')
  findOne(
    @Request() req,
    @Param('id') id: string
  ) {
    return this.codeService.findOne(req.user, id);
  }

  @Put(':id')
  update(
    @Request() req,
    @Param('id') id: string,
    @Body() updateCodeDto: UpdateCodeDto
  ) {
    return this.codeService.update(req.user, id, updateCodeDto);
  }

  @Delete(':id')
  remove(
    @Request() req,
    @Param('id') id: string
  ) {
    return this.codeService.remove(req.user, id);
  }
}
