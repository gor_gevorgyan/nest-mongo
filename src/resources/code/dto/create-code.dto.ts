import { IsNotEmpty } from 'class-validator';

export class CreateCodeDto {
  @IsNotEmpty()
  type: string

  @IsNotEmpty()
  value: string

  description: string
}
