import { PartialType } from '@nestjs/mapped-types';
import {  SignInDto} from '../../../auth/dto/signIn.dto';

export class UpdateUserDto extends PartialType(SignInDto) {}
