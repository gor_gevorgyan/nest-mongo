import { Injectable } from '@nestjs/common';
import { SignInDto } from '../../auth/dto/signIn.dto';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User, UserDocument } from '../../schemas/user.schema';

@Injectable()
export class UserService {
  constructor(@InjectModel(User.name) private userModel: Model<UserDocument>) {}

  async create(data: SignInDto): Promise<void> {
    await this.userModel.create({
      email: data.email,
      password: data.password
    });
  }

  async findByEmail(email: string): Promise<UserDocument> {
    let user = await this.userModel.findOne({ email });

    return user;
  }

  async findById(id: string): Promise<UserDocument> {
    let user = await this.userModel
      .findOne({_id: id})
      .select('-password');

    return user;
  }
}
