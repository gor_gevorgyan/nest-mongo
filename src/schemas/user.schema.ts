import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { isEmail } from 'class-validator';

export type UserDocument = User & Document;

@Schema({
  collection: 'users'
})
export class User {
  @Prop()
  name: string

  @Prop({
    required: true,
    validate: {
      validator: isEmail,
      message: '{VALUE} is not a valid email',
      isAsync: false
    }
  })
  email: string

  @Prop({
    required: true
  })
  password: string
}

export const UserSchema = SchemaFactory.createForClass(User);
