import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose  from 'mongoose';
import { User } from './user.schema';

export type BarCodeDocument = BarCode & mongoose.Document;

@Schema({
  collection: 'bar_code'
})
export class BarCode {
  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: 'users',
    required: true,
  })
  userId: User

  @Prop({
    required: true,
  })
  type: string

  @Prop({
    required: true
  })
  value: string

  @Prop()
  description: string
}

export const BarCodeSchema = SchemaFactory.createForClass(BarCode);
