import { IsEmail, Matches } from 'class-validator';
import { IsUniqueEmail } from '../../utils/validation/is-unique-email.decorator';

export class SignInDto {
  @IsEmail()
  @IsUniqueEmail()
  email: string

  @Matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/, {
    message: 'password is incorrect'
  })
  password: string
}
