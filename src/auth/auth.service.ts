import { Injectable } from '@nestjs/common';
import { UserService } from '../resources/user/user.service';
import { JwtService } from '@nestjs/jwt';
import { SignInDto } from './dto/signIn.dto';
import {
  hash,
  genSalt,
  compare
} from 'bcrypt';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UserService,
    private jwtService: JwtService,
    ) {}

  async validateUser(username: string, pass: string): Promise<any> {
    const user = await this.usersService.findByEmail(username);

    if (!user)
      return null;

    let check_password = await compare(pass, user.password);
    if (check_password)
      return {
        id: user._id
      };

    return null;
  }

  async login(user: any) {
    const payload = {
      sub: user.id
    };

    return {
      access_token: this.jwtService.sign(payload),
    };
  }

  async signIn(data: SignInDto) {
    const salt = await genSalt();
    data.password = await hash(data.password, salt);

    await this.usersService.create(data);

    return {
      success: true,
      message: 'thank you'
    };
  }
}
